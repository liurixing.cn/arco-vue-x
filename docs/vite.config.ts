import { defineConfig } from 'vite'
import VueJsx from '@vitejs/plugin-vue-jsx'
import { arcoVueIcon } from './plugins/arcoVueIcon.plugin'

export default defineConfig({
  plugins: [
    VueJsx(),
    arcoVueIcon()
  ],
  server: {
    port: 3100,
    host: true
  }
})
