# 快速开始

## 用法

### 全局安装组件库
```shell
npm i arco-vue-x --save --force
```

### 完整引入

```ts
# main.ts
import { createApp } from 'vue'
import ArcoVue from '@arco-design/web-vue';
import App from './App.vue';
import '@arco-design/web-vue/dist/arco.css';

const app = createApp(App);
app.use(ArcoVue);
app.mount('#app');
```
