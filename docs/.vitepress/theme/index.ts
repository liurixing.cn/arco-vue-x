import DefaultTheme from './theme-default'
import DemoPreview from './DemoPreview/index.vue';
import { EnhanceAppContext } from 'vitepress'
import ArcoVue from '@arco-design/web-vue';
import ArcoVueIcon from '@arco-design/web-vue/es/icon';
import ArcoVueX from '@arco-vue-x/arco-vue-x'
import '@arco-design/web-vue/dist/arco.css';


export default {
  ...DefaultTheme,
  enhanceApp(ctx: EnhanceAppContext) {
    ctx.app.use(ArcoVue)
    ctx.app.use(ArcoVueX)
    ctx.app.use(ArcoVueIcon)
    ctx.app.component('demo-preview', DemoPreview)
  }
}
