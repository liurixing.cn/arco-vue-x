
# ColmunsSettings

### 介绍

列设置抽屉组件，用于配置表格的列显示隐藏、固定列、排序等。

### 基本使用

需设置```table```组件的```scroll```为```{ y: '100%', x: '100%' }```
<preview path="../demos/colmuns-settings/colmuns-settings-1.vue" title="" description=" "></preview>

### 配置导入导出
事件：change-config

<preview path="../demos/colmuns-settings/colmuns-settings-4-config.vue" title="" description=" "></preview>


### 必须展示

在 ```initColmuns``` 中设置 ```mustShow```属性为true即必须展示这一列
<preview path="../demos/colmuns-settings/colmuns-settings-2-mustshow.vue" title="" description=" "></preview>


### 不展示

在 ```initColmuns``` 中设置 ```hidden```属性为true即可不展示该列
<preview path="../demos/colmuns-settings/colmuns-settings-3-hidden.vue" title="" description=" "></preview>


### 虚拟滚动不支持固定列操作

设置```virtual-list-props```属性给table后会开启虚拟滚动，不支持固定列操作，详见: [https://arco.design/vue/component/table#virtual-list](https://arco.design/vue/component/table#virtual-list)
<preview path="../demos/colmuns-settings/colmuns-settings-5-virsual.vue" title="" description=" "></preview>


### 默认列

<preview path="../demos/colmuns-settings/colmuns-settings-6-default.vue" title="" description=" "></preview>


## 组件 API

### Attributes 属性

| 参数 | 说明 | 类型 | 可选值 | 默认值 |
|  ----  | ----  | ----  | ----  | ----  |
| visible **(v-model)** | 列配置是否可见 | **boolean** | true/false | false |
| initColmuns **(v-model)** | 表格初始化列配置 | **ITableColumnData[]** | - | - |
| showColumns **(v-model)** | 当前表格展示的列配置 | **ITableColumnData[]** | - | - |
| tableRef **(v-model)** | 当前表格的domRef | **TableInstance** | - | - |
| width | 抽屉宽度 | **number** | - | - |

### ITableColumnData

| 参数 | 说明 | 类型 | 可选值 | 默认值 |
|  ----  | ----  | ----  | ----  | ----  |
| mustShow | 必须展示 | **boolean** | true/false | false |
| hidden | 是否隐藏这一列 | **boolean** | true/false | false |


### 事件

reset

