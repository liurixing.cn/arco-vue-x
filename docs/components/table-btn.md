
# TableBtn 表格按钮

### 基本使用

<preview path="../demos/table-btn/table-btn-1.vue" title="" description=" "></preview>

### 事件响应

<preview path="../demos/table-btn/table-btn-2-event.vue" title="" description=" "></preview>

### 类型

<preview path="../demos/table-btn/table-btn-3-type.vue" title="" description=" "></preview>

### 删除按钮自带二次确认

设置```:confirm="0"```可以屏蔽二次确认

<preview path="../demos/table-btn/table-btn-4-delete.vue" title="" description=" "></preview>


### 形状

<preview path="../demos/table-btn/table-btn-5-shape.vue" title="" description=" "></preview>

## 组件 API

### Attributes 属性

| 参数 | 说明 | 类型 | 可选值 | 默认值 |
|  ----  | ----  | ----  | ----  | ----  |
| outsize | 按钮宽高 | number | - | 32 |
| content | tooltip内容 | string | - | DEFAULT_TOOLTIP |
| type | 按钮类型 | IType | - | - |
| size | 图标宽高 | number | - | ```shape```=```square```时18，其余16 |
| confirm | 是否二次确认 | 0 、 1 | - | 0：不二次确认，否则type=delete时、confirmContent有值时二次确认 |
| confirmContent | 二次确认内容 | string | - | ```type```=```delete```时为```locale.confirmDel```字段，其余为```DEFAULT_TOOLTIP``` |
| disabled | 是否禁用 | boolean | - | false |
| loading | 是否展示loading | boolean | - | false |
| shape | 按钮形状 | IShape | 'round' 、 'circle' 、 'square' | ```circle``` |
| btnType | a-button的type属性 | string | 'primary' 、 'secondary' 、 'dashed' 、'outline'、'text'| primary |
| status | a-button的status属性 | string | 'normal' 、 'success' 、 'warning' 、 'danger'| ```type```=```delete```时为```danger```，其余为 ```normal``` |
| position | a-tooltip的position属性 | string | | 'top'、 'tl'、 'tr'、 'bottom'、 'bl'、 'br'、 'left'、 'lt'、 'lb'、 'right'、 'rt'、 'rb' | - |


### DEFAULT_TOOLTIP 默认tooltip内容

| 类型 | 默认值 |
|  ----  | ----  |
|confirm | 确认 |
|cancel | 取消 |
|detail | 详情 |
|edit | 编辑 |
|delete | 删除 |
|download | 下载 |
|group | 组成员 |
|stop | 停用 |
|activation | 激活 |
|enable | 启用 |
|sync | 同步 |
|upload | 上传 |
|add | 新增 |
|search | 查询 |
|refresh | 刷新 |
|export | 导出 |
|import | 导入 |
|save | 保存 |
|location | 定位 |

### IType

| 类型 | 说明 |
|  ----  | ----  |
|confirm | 确认 |
|cancel | 取消 |
|detail | 详情 |
|edit | 编辑 |
|delete | 删除 |
|download | 下载 |
|group | 组成员 |
|stop | 停用 |
|activation | 激活 |
|enable | 启用 |
|sync | 同步 |
|upload | 上传 |
|add | 新增 |
|search | 查询 |
|refresh | 刷新 |
|export | 导出 |
|import | 导入 |
|save | 保存 |
|location | 定位 |


### Events 事件

| 事件名 | 说明 | 参数 | 返回值 |
|  ----  | ----  | ----  | ----  |
| click | 点击按钮时触发 |  |  |
