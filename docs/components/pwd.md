
# Pwd pwd

## 基本使用

<preview path="../demos/pwd/pwd-1.vue" title="" description=" "></preview>

## 组件 API

### Attributes 属性

| 参数 | 说明 | 类型 | 可选值 | 默认值 |
|  ----  | ----  | ----  | ----  | ----  |
| list | 判断是否合法 | ICheck[] |  | [] |
| value | 密码值 | string |  | '' |
| popupOffset | 弹出框的偏移量（弹出框距离触发器的偏移距离） | number |  | 10 |
| showStrengthOnValidation | 只在满足所有条件时，才展示密码强度 | boolean | true/false | true |
| position | 弹框位置 | string | 'top'、 'tl'、 'tr'、 'bottom'、 'bl'、 'br'、 'left'、 'lt'、 'lb'、 'right'、 'rt'、 'rb' | 'bottom' |

### ICheck

| 名称 | 类型 | 说明 |
|  ----  | ----  | ----  |
|label | string | 文字 |
|fn | (value: string)=> boolean | 判断是否符合要求 |
|tooltip | string | 在文案后展示问号，鼠标移入问号展示的文案 |


### Slots 插槽

| 插槽名 | 说明 | 参数 |
|  ----  | ----  | ----  |
| 默认插槽 | 放入password |  |
