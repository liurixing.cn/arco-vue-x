const replaceArcoDesignVue = (str) =>
  str.replace(/(@arco-design\/web-vue)\/es(\/icon)/, '$1/lib$2')

export const arcoVueIcon = () => {
  return {
    name: 'transform-file',
    apply: 'build' as 'build' | 'serve',
    transform (src) {
      if (src.includes('@arco-design/web-vue/es/icon')) {
        return replaceArcoDesignVue(src)
      }
    }
  }
}
