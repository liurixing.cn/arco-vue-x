---
layout: home

title: 选项卡标题
titleTemplate: 选项卡描述
editLink: true
lastUpdated: true
hero:
  name: arco-vue-x
  text:
  tagline: A Vue 3.0 UI Library based on arco-design-vue
  image:
    src: /logo.png
    alt: logo
  actions:
    - theme: brand
      text: 快速开始
      link: /guide/
    - theme: alt
      text: 组件
      link: /components/colmuns-settings
features:
  - icon: 🔨
    title: 功能/特点 1
    details: 功能/特点 1 具体描述信息。
  - icon: 🧩
    title: 功能/特点 2
    details: 功能/特点 2 具体描述信息。
  - icon: ✈️
    title: 功能/特点 3。
    details: 功能/特点 3 具体描述信息。
---
