import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import ArcoVue from '@arco-design/web-vue'
import ArcoVueIcon from '@arco-design/web-vue/es/icon'
import '@arco-design/web-vue/dist/arco.css'
import ArcoVueX from '@arco-vue-x/arco-vue-x'

const env = import.meta.env
console.log(env)

const app = createApp(App)
app.use(ArcoVue)
app.use(ArcoVueX)
app.use(ArcoVueIcon)
app.mount('#app')
