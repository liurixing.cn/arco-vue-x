export const Config = {
  /** 组件名的前缀 */
  COMPONENT_PREFIX: 'x',
  /** 组件库名称 */
  COMPONENT_LIB_NAME: 'arco-vue-x'
}
