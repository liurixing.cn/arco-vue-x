import Pwd from '@arco-vue-x/pwd'
import TableBtn from '@arco-vue-x/table-btn'
import Table from '@arco-vue-x/table'
import ColmunsSettings from '@arco-vue-x/colmuns-settings'
import { App } from 'vue'

const components = [
  ColmunsSettings,
  Table,
  TableBtn,
  Pwd
] // components

// 全局动态添加组件
const install = (app: App): void => {
  components.forEach(component => {
    app.component(component.name as string, component)
  })
}

export default {
  install
}
