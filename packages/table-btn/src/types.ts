import { ExtractPropTypes } from 'vue'

export const tableBtnProps = {
} as const

export type TableBtnProps = ExtractPropTypes<typeof tableBtnProps>
