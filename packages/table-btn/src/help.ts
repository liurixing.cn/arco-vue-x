import {
  IconDelete,
  IconDownload,
  IconEdit,
  IconExport,
  IconEye,
  IconImport,
  IconPause,
  IconPlayArrow,
  IconPlus,
  IconRecord,
  IconRefresh,
  IconSave,
  IconSearch,
  IconSwap,
  IconUpload,
  IconUserGroup,
  IconLocation
} from '@arco-design/web-vue/es/icon'

export type IShape = 'round' | 'circle' | 'square' | undefined;
export type IType =
  | 'import'
  | 'detail'
  | 'edit'
  | 'download'
  | 'delete'
  | 'group'
  | 'stop'
  | 'activation'
  | 'enable'
  | 'sync'
  | 'upload'
  | 'add'
  | 'export'
  | 'refresh'
  | 'search';

export interface ILocale {
  // 确认删除
  confirmDel: string;
  // 提示
  tips: string;
  // 确认
  confirm: string;
  // 取消
  cancel: string;
  // 详情
  detail: string;
  // 编辑
  edit: string;
  // 删除
  delete: string;
  // 下载
  download: string;
  // 组成员
  group: string;
  // 停用
  stop: string;
  // 激活
  activation: string;
  // 启用
  enable: string;
  // 同步
  sync: string;
  // 上传
  upload: string;
  // 新增
  add: string;
  // 查询
  search: string;
  // 刷新
  refresh: string;
  // 导出
  export: string;
  // 导入
  import: string;
  // 保存
  save: string;
  // 定位
  location: string,
}

export const DEFAULT_VALUE = {
  shape: 'circle' as IShape,
  isBold: true,
  strokeWidth: 5
}

export const TYPE_MATCH = {
  detail: {
    icon: IconEye
  },
  edit: {
    icon: IconEdit
  },
  delete: {
    icon: IconDelete
  },
  download: {
    icon: IconDownload
  },
  group: {
    icon: IconUserGroup
  },
  // 停用
  stop: {
    icon: IconPause
  },
  // 激活
  activation: {
    icon: IconPlayArrow
  },
  // 启用
  enable: {
    icon: IconRecord
  },
  sync: {
    icon: IconSwap
  },
  upload: {
    icon: IconUpload
  },
  add: {
    icon: IconPlus
  },
  search: {
    icon: IconSearch
  },
  refresh: {
    icon: IconRefresh
  },
  export: {
    icon: IconExport
  },
  import: {
    icon: IconImport
  },
  save: {
    icon: IconSave
  },
  location: {
    icon: IconLocation
  }
}
