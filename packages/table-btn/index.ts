import TableBtn from './src/table-btn.vue'
import { App } from 'vue'

TableBtn.name = 'x-table-btn'

TableBtn.install = (app: App): void => {
  // 注册组件
  app.component(TableBtn.name as string, TableBtn)
}

export default TableBtn
