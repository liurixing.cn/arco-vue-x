import Table from './src/table.vue'
import { App } from 'vue'

Table.name = 'x-table'

Table.install = (app: App): void => {
  // 注册组件
  app.component(Table.name as string, Table)
}

export default Table
