import { onMounted, onUnmounted } from 'vue'

/**
 * 监听target的属性变化
 * @param target
 * @param attrName
 * @param callback
 */
export const useListenAttr = (
  target: HTMLElement,
  attrName: string,
  callback: (value: any) => void
) => {
  const callbackFunc = () => callback((target as HTMLElement).getAttribute(attrName))

  const observer = new MutationObserver((mutationsList) => {
    const langChange = mutationsList.find(
      (item) => item.attributeName === attrName
    )
    if (!langChange) {
      return
    }
    callbackFunc()
  })

  onMounted(() => {
    observer.observe(target, { attributes: true })
    callbackFunc()
  })

  onUnmounted(() => {
    observer.disconnect()
  })
}
