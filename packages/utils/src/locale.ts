import { Ref, ref } from 'vue'
import { useListenAttr } from './listen'

const match: Record<string, string> = {
  // 中文
  'zh-cn': 'zhCN',
  zhCN: 'zhCN',
  'cn-zh': 'zhCN',
  cn: 'zhCN',
  zh: 'zhCN',
  // 繁体
  'zh-tw': 'zhTW',
  tw: 'zhTW',
  // 英文
  en: 'en',
  us: 'en',
  enus: 'en',
  en_us: 'en',
  'en-US': 'en',
  // 俄文
  'ru-RU': 'ru',
  ruru: 'ru',
  ru: 'ru'
}

export const getLocale = (lang: string): string => {
  return match[lang.toLocaleLowerCase()] || 'zhCN'
}

export const useLocale = <T>(locales: { zhCN?: T; zhTW?: T; en?: T; ru?: T }) => {
  const locale = ref({}) as Ref<T>

  useListenAttr(
    document.querySelector('html') as HTMLElement,
    'lang',
    (_lang) => {
      locale.value = locales[getLocale(_lang) as 'zhCN' | 'zhTW' | 'en' | 'ru'] as T
    }
  )

  return {
    locale
  }
}
