
/**
 * 判断两个数组是否相等
 */
export const isArrEqual = (arr1: string[], arr2: string[]): boolean => {
  if (arr1.length !== arr2.length) {
    return false
  }
  arr1 = arr1.slice().sort()
  arr2 = arr2.slice().sort()
  for (let i = 0; i < arr1.length; i++) {
    if (arr1[i] !== arr2[i]) {
      return false
    }
  }
  return true
}

/**
 * 判断数组1是不是包含数据2
 */
export const isArrIncluded = (dad: string[], son: string[]): boolean => {
  for (let i = 0; i < son.length; i++) {
    if (!dad.includes(son[i])) {
      return false
    }
  }
  return true
}

/**
 * 从数组1中删除数组2拥有的数据
 */
export const minusArr = (arr1: string[], arr2: string[]) => {
  return arr1.filter((item) => !arr2.includes(item))
}

/**
 * 合并两个数组
 */
export const plusArr = (arr1: string[], arr2: string[], unique = false) => {
  const res = [...arr1, ...arr2]
  return unique ? [...new Set(res)] : res
}
