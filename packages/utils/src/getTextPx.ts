export const getTextPx = (str: string): number => {
  const cancas = document.createElement('canvas')
  const context = cancas.getContext('2d') as CanvasRenderingContext2D
  context.font =
    '500 14px Inter, "-apple-system", BlinkMacSystemFont, "PingFang SC", "Hiragino Sans GB", "noto sans", "Microsoft YaHei", "Helvetica Neue", Helvetica, Arial, sans-serif'
  const metrics = context.measureText(str)
  return metrics.width + 1
}

export const getInputSearchWidth = (text: string) => {
  const w = getTextPx(text) + 58
  return w < 200 ? 200 : w
}

export const getTableColWidth = (text: string, extraWInfo: any = {}) => {
  const { isSort = false, isFilter = false } = extraWInfo
  const extraW = 32 + (isSort ? 22 : 0) + (isFilter ? 20 : 0)
  const w = getTextPx(text) + extraW
  return w < 120 ? 120 : w
}
