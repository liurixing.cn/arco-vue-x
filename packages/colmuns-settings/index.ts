import ColmunsSettings from './src/colmuns-settings.vue'
import { App } from 'vue'

ColmunsSettings.name = 'x-colmuns-settings'

ColmunsSettings.install = (app: App): void => {
  // 注册组件
  app.component(ColmunsSettings.name as string, ColmunsSettings)
}

export default ColmunsSettings
