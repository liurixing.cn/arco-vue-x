import {
  Message,
  TableColumnData,
  TableInstance,
  TableRowSelection
} from '@arco-design/web-vue'
import {
  ComputedRef,
  Ref,
  computed,
  nextTick,
  onMounted,
  ref,
  watch
} from 'vue'
import {
  getTableColWidth,
  isArrIncluded,
  minusArr,
  plusArr
} from '@arco-vue-x/utils'

export interface ILocale {
  title: string;
  fixedLeft: string;
  fixedRight: string;
  noFixed: string;
  columnTitle: string;
  searchPlaceHolder: string;
  reset: string;
  mustShowInfo: string;
}

export interface ITableColumnData extends TableColumnData {
  // 必须展示
  mustShow?: boolean;
  // 是否隐藏这一列
  hidden?: boolean;
  // 禁止选择
  disabled?: boolean;
}

export const useSelect = (
  contentRef: Ref<HTMLDivElement>,
  leftRenderColumnsList: Ref<ITableColumnData[]>,
  renderColumnsList: Ref<ITableColumnData[]>,
  rightRenderColumnsList: Ref<ITableColumnData[]>,
  oldSelectColumns: ITableColumnData[],
  getVisible: () => boolean,
  infoOnMustShow: string
) => {
  const selectedKeys = ref<string[]>([])

  const mustShowKeys = oldSelectColumns
    .filter((item) => item.mustShow)
    .map((item) => item.dataIndex as string)

  const setSelectKeys = (keys: string[]) => {
    selectedKeys.value = plusArr(keys, mustShowKeys)
  }

  // 点击checkbox时
  const handleSelect = (
    rowKeys: (string | number)[],
    rowkey: string | number
  ) => {
    if (mustShowKeys.includes(rowkey as string)) {
      Message.warning(infoOnMustShow)
      return
    }
    setSelectKeys(rowKeys as string[])
  }

  const dealWith = (arr: string[]) => {
    const isSon = isArrIncluded(selectedKeys.value, arr)
    setSelectKeys(
      (isSon ? minusArr : plusArr)(selectedKeys.value, arr, true) as string[]
    )
  }

  // 点击行时
  const handleRowClick = (v: ITableColumnData) => {
    const current = v.dataIndex
    if (mustShowKeys.includes(current as string)) {
      Message.warning(infoOnMustShow)
      return
    }
    dealWith([current as string])
  }

  // 点击表头时
  const handleHeaderClick = (list: Ref<ITableColumnData[]>) => {
    const renderKeys = list.value.map((item) => item.dataIndex as string)
    dealWith(renderKeys)
  }

  const initData = () => {
    setSelectKeys(
      oldSelectColumns
        .filter((item) => item.mustShow || !item.hidden)
        .map(({ dataIndex }) => dataIndex as string)
    )
  }

  const listener = (e: Event) => {
    const heads = contentRef.value?.querySelectorAll('thead')
    const leftContainer = contentRef.value?.querySelector(
      'div[data-fixed="left"]'
    )
    const centerContainer = contentRef.value?.querySelector(
      'div[data-fixed="center"]'
    )
    const rightContainer = contentRef.value?.querySelector(
      'div[data-fixed="right"]'
    )
    const is = Array.from(heads).some((item) =>
      item.contains(e.target as Node)
    )
    const isLeft = leftContainer?.contains(e.target as Node)
    const isCenter = centerContainer?.contains(e.target as Node)
    const isRight = rightContainer?.contains(e.target as Node)
    if (is) {
      isLeft && handleHeaderClick(leftRenderColumnsList)
      isCenter && handleHeaderClick(renderColumnsList)
      isRight && handleHeaderClick(rightRenderColumnsList)
      e.stopPropagation()
    }
  }

  const addEventListener = () => {
    nextTick(() => {
      contentRef.value?.addEventListener('click', listener)
    })
  }

  const removeListener = () => {
    contentRef.value.removeEventListener('click', listener)
  }

  watch(getVisible, () => {
    if (getVisible()) {
      addEventListener()
    } else {
      removeListener()
    }
  })

  onMounted(() => {
    initData()
  })

  return {
    selectedKeys,
    initSelect: initData,
    handleSelect,
    handleRowClick,
    handleHeaderClick
  }
}

export const useSearch = (
  columnsGroup: Ref<Record<'left' | 'right' | 'center', ITableColumnData[]>>,
  leftRenderColumnsList: Ref<ITableColumnData[]>,
  renderColumnsList: Ref<ITableColumnData[]>,
  rightRenderColumnsList: Ref<ITableColumnData[]>
) => {
  const searchWords = ref('')
  const handleSearchInput = () => {
    const filterColumns = (params: ITableColumnData) =>
      searchWords.value
        ? (params.title as string)
            .toLowerCase()
            .includes(searchWords.value.toLowerCase())
        : true

    renderColumnsList.value = columnsGroup.value.center.filter(filterColumns)
    leftRenderColumnsList.value = columnsGroup.value.left.filter(filterColumns)
    rightRenderColumnsList.value =
      columnsGroup.value.right.filter(filterColumns)
  }

  return {
    searchWords,
    renderColumnsList,
    handleSearchInput
  }
}

export const rowSelection: TableRowSelection = {
  type: 'checkbox',
  showCheckedAll: true,
  onlyCurrent: false
}

export const COLUMNS = {
  dataIndex: 'title',
  slotName: 'title',
  tooltip: true,
  ellipsis: true
}

export const settingsColumn: ITableColumnData = {
  titleSlotName: 'settingsTitle',
  slotName: '__settings',
  dataIndex: '__settings',
  align: 'center',
  fixed: 'right',
  width: 35
}

export const calcConfig = (newColumns: any[]) =>
  newColumns.reduce(
    (total: any, current: any, index: number) => ({
      ...total,
      [current.dataIndex as string]: {
        fixed: current.fixed,
        hidden: current.hidden,
        sort: index
      }
    }),
    {}
  )

export const calculateShowColumns = (columns: ITableColumnData[]) =>
  columns
    .filter((item) => !item.hidden || item.mustShow)
    .map((item) => ({
      ...item,
      width: item.width || getTableColWidth(item.title as string)
    }))
    // @ts-ignore
    .concat(settingsColumn)

export const useColumnsSettings = (columns: Ref<ITableColumnData[]>) => {
  const visible = ref(false)

  const showColumns = computed(() => {
    return (
      columns.value
        .filter((item) => !item.hidden || item.mustShow)
        .map((item) => ({
          ...item,
          width: item.width || getTableColWidth(item.title as string)
        }))
        // @ts-ignore
        .concat(settingsColumn)
    )
  })

  return {
    visible,
    showColumns
  }
}

export interface IGroup {
  left: ITableColumnData[];
  center: ITableColumnData[];
  right: ITableColumnData[];
}

export const dealWithInitData = (
  columns: ITableColumnData[],
  nogroup: boolean
): IGroup => {
  const allColumns = columns.map((item) => ({
    ...item,
    disabled: item.mustShow
  }))
  return allColumns.reduce(
    (total, current) => {
      const index = nogroup
        ? 'center'
        : {
            left: 'left',
            undefined: 'center',
            null: 'center',
            right: 'right'
          }[`${current.fixed}`]
      total[index as 'left' | 'center' | 'right'].push(current)
      return total
    },
    { left: [], center: [], right: [] } as {
      left: ITableColumnData[];
      center: ITableColumnData[];
      right: ITableColumnData[];
    }
  )
}

export const useFixedColumns = (
  renderColumnsList: Ref<ITableColumnData[]>,
  leftRenderColumnsList: Ref<ITableColumnData[]>,
  rightRenderColumnsList: Ref<ITableColumnData[]>
) => {
  const gotoDirection = (
    from: 'left' | 'center' | 'right',
    column: ITableColumnData,
    to: 'left' | 'center' | 'right'
  ) => {
    if (to === 'center') {
      delete column.fixed
      renderColumnsList.value = renderColumnsList.value.concat(column)
    }
    if (to === 'left') {
      column.fixed = to
      leftRenderColumnsList.value = leftRenderColumnsList.value.concat(column)
    }
    if (to === 'right') {
      column.fixed = to
      rightRenderColumnsList.value =
        rightRenderColumnsList.value.concat(column)
    }

    if (from === 'center') {
      renderColumnsList.value = renderColumnsList.value.filter(
        (item) => item.dataIndex !== column.dataIndex
      )
    }
    if (from === 'right') {
      rightRenderColumnsList.value = rightRenderColumnsList.value.filter(
        (item) => item.dataIndex !== column.dataIndex
      )
    }
    if (from === 'left') {
      leftRenderColumnsList.value = leftRenderColumnsList.value.filter(
        (item) => item.dataIndex !== column.dataIndex
      )
    }
  }

  return {
    gotoDirection
  }
}

export const useInit = (
  getInitColmuns: () => ITableColumnData[],
  getTableRef: () => TableInstance | undefined,
  getLocale: () => ILocale
) => {
  const columns: Ref<ITableColumnData[]> = ref(getInitColmuns())
  // 左右固定列数组
  const leftRenderColumnsList = ref<ITableColumnData[]>([])
  const renderColumnsList = ref<ITableColumnData[]>([])
  const rightRenderColumnsList = ref<ITableColumnData[]>([])

  // 被设置的表格是否开启了虚拟列表
  const enableVirtual = computed(() => {
    return !!getTableRef()?.virtualListProps
  })

  // 把 columns 处理成左右固定列的数组
  const columnsGroup = computed(() =>
    dealWithInitData(columns.value, enableVirtual.value)
  )

  const initData = () => {
    leftRenderColumnsList.value = columnsGroup.value.left || []
    renderColumnsList.value = columnsGroup.value.center || []
    rightRenderColumnsList.value = columnsGroup.value.right || []
  }

  const ROW_SELECTION: Ref<TableRowSelection> = ref({
    type: 'checkbox',
    showCheckedAll: true,
    onlyCurrent: false
  })

  const TABLE_COLUMNS: ComputedRef<ITableColumnData[]> = computed(() => [
    {
      ...COLUMNS,
      title: getLocale().columnTitle
    }
  ])

  return {
    TABLE_COLUMNS,
    ROW_SELECTION,
    columns,
    initData,
    leftRenderColumnsList,
    renderColumnsList,
    rightRenderColumnsList,
    enableVirtual,
    columnsGroup
  }
}

export const useDrag = (
  leftRenderColumnsList: Ref<ITableColumnData[]>,
  renderColumnsList: Ref<ITableColumnData[]>,
  rightRenderColumnsList: Ref<ITableColumnData[]>
) => {
  const handleLeftChange = (data: ITableColumnData[]) => {
    leftRenderColumnsList.value = data
  }
  const handleCenterChange = (data: ITableColumnData[]) => {
    renderColumnsList.value = data
  }
  const handleRightChange = (data: ITableColumnData[]) => {
    rightRenderColumnsList.value = data
  }
  return {
    handleLeftChange,
    handleCenterChange,
    handleRightChange
  }
}
