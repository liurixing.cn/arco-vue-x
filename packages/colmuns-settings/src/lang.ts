import { ILocale } from './help'

export const zhCN: ILocale = {
  title: '列设置',
  fixedLeft: '固定在左侧',
  fixedRight: '固定在右侧',
  noFixed: '不固定',
  columnTitle: '列名',
  searchPlaceHolder: '请输入',
  reset: '重置',
  mustShowInfo: '必须展示'
}

export const zhTW: ILocale = {
  title: '列設定',
  fixedLeft: '固定在左側',
  fixedRight: '固定在右側',
  noFixed: '不固定',
  columnTitle: '列名',
  searchPlaceHolder: '請輸入',
  reset: '重置',
  mustShowInfo: '必須展示'
}

export const en: ILocale = {
  title: 'Column Settings',
  fixedLeft: 'Fixed on the left',
  fixedRight: 'Fixed on the right',
  noFixed: 'Not fixed',
  columnTitle: 'Column Name',
  searchPlaceHolder: 'Please enter',
  reset: 'Reset',
  mustShowInfo: 'Must show'
}

export const ru: ILocale = {
  title: 'Параметры столбца',
  fixedLeft: 'Зафиксировано слева',
  fixedRight: 'Зафиксировано справа',
  noFixed: 'Не фиксировано',
  columnTitle: 'Название столбца',
  searchPlaceHolder: 'Пожалуйста, введите',
  reset: 'Сброс',
  mustShowInfo: 'Должен показать'
}
