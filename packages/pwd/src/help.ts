import { zxcvbn, zxcvbnOptions } from '@zxcvbn-ts/core'
import * as zxcvbnCommonPackage from '@zxcvbn-ts/language-common'
import * as zxcvbnEnPackage from '@zxcvbn-ts/language-en'

/* eslint-disable no-unused-vars */
export interface ICheck {
  label: string;
  tooltip?: string;
  fn: (value: string) => boolean;
}

export enum IStatus {
  normal,
  danger,
  warn,
  success,
}

export interface ILocale {
  // 密码强度
  pwdStrength: string;
  // 高
  high: string;
  // 中
  medium: string;
  // 低
  low: string;
}

const options = {
  translations: zxcvbnEnPackage.translations,
  graphs: zxcvbnCommonPackage.adjacencyGraphs,
  dictionary: {
    ...zxcvbnCommonPackage.dictionary,
    ...zxcvbnEnPackage.dictionary
  }
}

zxcvbnOptions.setOptions(options)

export const judgePwdStrength = (pwd: string) => {
  const score = zxcvbn(pwd).guessesLog10
  if (score > 0 && score < 6) {
    return IStatus.danger
  }
  if (score >= 6 && score < 10) {
    return IStatus.warn
  }
  return IStatus.success
}
