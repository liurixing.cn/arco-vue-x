import { ILocale } from './help'

export const en: ILocale = {
  pwdStrength: 'Password Strength:',
  high: 'High',
  medium: 'Medium',
  low: 'Low'
}

export const zhCN: ILocale = {
  pwdStrength: '密码强度：',
  high: '高',
  medium: '中',
  low: '低'
}

export const zhTW: ILocale = {
  pwdStrength: '密碼強度：',
  high: '高',
  medium: '中',
  low: '低'
}

export const ru: ILocale = {
  pwdStrength: 'Сила пароля:',
  high: 'Высокая',
  medium: 'Средняя',
  low: 'Низкая'
}
