import Pwd from './src/pwd.vue'
import { App } from 'vue'

Pwd.name = 'x-pwd'

Pwd.install = (app: App): void => {
  // 注册组件
  app.component(Pwd.name as string, Pwd)
}

export default Pwd
